use combine::{between, choice, combinator::parser, parser::char::spaces, stream::easy, token, ParseError, Parser, Stream};
use nom::{
    branch::alt,
    bytes::complete::{tag, take_till1, take_while},
    combinator::{map, peek},
    error::ErrorKind,
    sequence::{delimited, preceded},
    IResult,
};
use std::{
    cmp::Ordering,
    collections::{hash_map::Entry, BinaryHeap, HashMap},
    rc::Rc,
};

#[derive(Debug)]
pub struct Srs {
    rules: BinaryHeap<CompiledRule>,
    literals: BidiStringIdMap,
    variables: BidiStringIdMap,
}

impl Srs {
    pub fn new() -> Self {
        Srs {
            rules: BinaryHeap::new(),
            literals: BidiStringIdMap::new(),
            variables: BidiStringIdMap::new(),
        }
    }

    pub fn add_rule(&mut self, rule: Rule) {
        let rule = RuleCompiler::new(&mut self.literals, rule);
        self.rules.push(rule.compile());
    }

    pub fn process(&mut self, mut input: &str) -> String {
        let (_,expr) = self.parse_expression(&mut input);
        let expr = expr.unwrap();
        for rule in &self.rules {

        }
        String::new()
    }

    fn match_rule(rule: &CompiledRule, expr: &ParsedExpr) {
        //TODO:
        match (&rule.from, expr) {
            (CompiledExpr::Term(_), ParsedExpr::Term(_)) => {},
            (CompiledExpr::Term(_), ParsedExpr::Literal(_)) => {},
            (CompiledExpr::Literal(_), ParsedExpr::Term(_)) => {},
            (CompiledExpr::Literal(_), ParsedExpr::Literal(_)) => {},
            (CompiledExpr::Variable(_), ParsedExpr::Term(_)) => {},
            (CompiledExpr::Variable(_), ParsedExpr::Literal(_)) => {},
        }
    }

    fn parse_expression<'a>(&mut self, mut input: &'a str) -> (&'a str, Option<ParsedExpr>) {
        let mut exprs = Vec::new();
        loop {
            let (inp, token) = take_while::<_, &str, ()>(|c| c == ' ')(input).unwrap();
            input = inp;
            if (*input).is_empty() || tag::<&str, &str, ()>(")")(input).is_ok() {
                match exprs.len() {
                    0 => return (input, None),
                    1 => return (input, Some( exprs.pop().unwrap())),
                    _ => return (input, Some(ParsedExpr::Term(exprs))),
                }
            }
            let res = tag::<&str, &str, ()>("(")(input);
            match res {
                Ok((mut inp, expr)) => {
                    let (inp, expr) = self.parse_expression(&mut inp);
                    input = inp;
                    let (inp, _) = tag::<&str, &str, ()>(")")(input).unwrap();
                    input = inp;
                    if let Some(expr) = expr {
                        exprs.push(expr);
                        input = inp;
                    }
                }
                Err(_) => {
                    let (inp, token) = take_till1::<_, &str, ()>(|c| c == ' ' || c == ')' || c == '(' )(input).unwrap();
                    input = inp;
                    exprs.push(ParsedExpr::Literal(self.literals.get_id(token.to_owned())));
                }
            };
        }
    }
}

struct RuleMatcher {
    variable_assignments: HashMap<usize, ParsedExpr>
}

#[derive(Debug)]
struct RuleCompiler<'a> {
    literals: &'a mut BidiStringIdMap,
    variables: StringIdMap,
    rule: Rule,
}

impl<'a> RuleCompiler<'a> {
    pub fn new(literals: &'a mut BidiStringIdMap, rule: Rule) -> Self {
        RuleCompiler {
            literals,
            variables: StringIdMap::new(),
            rule,
        }
    }

    pub fn compile(mut self) -> CompiledRule {
        let from = self.compile_from_expr(self.rule.from.clone());
        let into = self.compile_into_expr(self.rule.into.clone());
        CompiledRule::new(self.rule.priority, from, into.unwrap())
    }

    fn compile_from_expr(&mut self, expr: Expr) -> CompiledExpr {
        match expr {
            Expr::Term(exprs) => CompiledExpr::Term(exprs.into_iter().map(|expr| self.compile_from_expr(expr)).collect()),
            Expr::Literal(lit) => CompiledExpr::Literal(self.literals.get_id(lit)),
            Expr::Variable(var) => CompiledExpr::Variable(self.variables.get_id(var)),
        }
    }

    fn compile_into_expr(&mut self, expr: Expr) -> Result<CompiledExpr, UnknownVariableId> {
        Result::Ok(match expr {
            Expr::Term(exprs) => CompiledExpr::Term({
                let mut cexprs = Vec::new();
                for expr in exprs.into_iter() {
                    cexprs.push(self.compile_into_expr(expr)?)
                }
                cexprs
            }),
            Expr::Literal(lit) => CompiledExpr::Literal(self.literals.get_id(lit)),
            Expr::Variable(var) => CompiledExpr::Variable(self.variables.get_id_final(var)?),
        })
    }
}

#[derive(Debug)]
pub enum ParsedExpr {
    Term(Vec<ParsedExpr>),
    Literal(usize),
}

#[derive(Clone, Debug)]
pub enum Expr {
    Term(Vec<Expr>),
    Literal(String),
    Variable(String),
}

impl Expr {
    pub fn term(exprs: Vec<Expr>) -> Self {
        Expr::Term(exprs)
    }

    pub fn lit(s: &str) -> Self {
        Expr::Literal(s.to_owned())
    }

    pub fn var(s: &str) -> Self {
        Expr::Variable(s.to_owned())
    }
}

#[derive(Eq, PartialEq, Debug)]
pub struct CompiledRule {
    priority: usize,
    from: CompiledExpr,
    into: CompiledExpr,
}

impl CompiledRule {
    fn new(priority: usize, from: CompiledExpr, into: CompiledExpr) -> Self {
        CompiledRule {
            priority,
            from,
            into,
        }
    }
}

impl Ord for CompiledRule {
    fn cmp(&self, other: &Self) -> Ordering {
        self.priority.cmp(&other.priority)
    }
}

impl PartialOrd for CompiledRule {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(&other))
    }
}

#[derive(Debug)]
pub struct Rule {
    priority: usize,
    from: Expr,
    into: Expr,
}

impl Rule {
    pub fn new(priority: usize, from: Expr, into: Expr) -> Self {
        Rule {
            priority,
            from,
            into,
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum CompiledExpr {
    Term(Vec<CompiledExpr>),
    Literal(usize),
    Variable(usize),
}

#[derive(Debug)]
struct BidiStringIdMap {
    vec: Vec<Rc<String>>,
    map: HashMap<Rc<String>, usize>,
}

impl BidiStringIdMap {
    pub fn new() -> Self {
        BidiStringIdMap {
            vec: Vec::new(),
            map: HashMap::new(),
        }
    }

    pub fn get_id(&mut self, string: String) -> usize {
        let id = self.vec.len();
        let string = Rc::new(string);
        let entry = self.map.entry(string.clone());
        match entry {
            Entry::Occupied(occ) => *occ.get(),
            Entry::Vacant(vac) => {
                self.vec.push(string);
                *vac.insert(id)
            }
        }
    }

    pub fn get_string(&self, id: usize) -> &str {
        &self.vec[id]
    }
}

#[derive(Debug)]
struct StringIdMap {
    map: HashMap<String, usize>,
    counter: usize,
}

impl StringIdMap {
    pub fn new() -> Self {
        StringIdMap {
            map: HashMap::new(),
            counter: 0,
        }
    }

    pub fn get_id(&mut self, string: String) -> usize {
        *self.map.entry(string).or_insert({
            let id = self.counter;
            self.counter += 1;
            id
        })
    }

    pub fn get_id_final(&self, string: String) -> Result<usize, UnknownVariableId> {
        self.map.get(&string).map(|v| *v).ok_or(UnknownVariableId)
    }
}

#[derive(Debug)]
struct UnknownVariableId;
