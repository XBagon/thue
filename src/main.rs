use crate::srs::{Expr, Rule, Srs};

mod srs;

fn main() {
    let mut srs = Srs::new();
    srs.add_rule(Rule::new(
        100,
        Expr::term(vec![Expr::var("Y"), Expr::lit("+"), Expr::var("X")]),
        Expr::term(vec![Expr::var("Y"), Expr::lit("+"), Expr::var("X")]),
    ));
    //dbg!(&srs);
    srs.process(&String::from("() 52 + 15 - 53 / (5 + 1 + 3) - 42 / (3 + 1)- 5/(2)"));
}
